var foo = function (a, cb) {
  setTimeout(function () {
    console.log("inside foo", a);
    //console.log("inside foo - cb", cb.toString());
    cb(null);
  }, 1000);
};

var bar = function (b, d, e, cb) {
  setTimeout(function () {
    console.log("inside bar", b, d, e);
    cb('err bar');
  }, 1);
}

var then = function (c, cb) {
  setTimeout(function () {
    console.log('inside then', c);
    cb();
  }, 2000);
}

var r = require("./ripple");
var params = {
  hi: 'hello',
  meow: 'bow'
}
funs = [
  {
    name: foo,
    params: [params.hi]
  },
  {
    name: bar,
    params: [params.meow, params.hi, 'emil']
  },
  {
    name: then,
    params: [params]
  }
];

var final = function (err, data) {
  if (err) {
    console.log('error handler here', err);
  } else {
    console.log('ripple ended without errors', data);
  }
}
//r.ripple(params, [foo, bar, then]);
r.ripple(params, funs, final);
//r.ripple(params, [{foo : 1}, {bar: 2}, {then: 3}]);


