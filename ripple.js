function ripple () {

  // TODO make sure the function calls args[l].params that is provided with the params
  // ie, the first argument to ripple

  var argsArray = Array.prototype.slice.call(arguments, 0);
  var params = argsArray.shift();
  var args = argsArray.shift();
  var final = argsArray.shift();

  function tangle (scope, args) {
    // used in place of bind.
    var fn = this;
    return function () {
      fn.apply(scope, args);
    }
  }

  Function.prototype.tangle = tangle; 

  function pack(params, fn) {
    // pack both the arguments into a single array. 
    // params will already be an array
    var ar = [];

    for (var i = 0; i < params.length; i++) {
      ar.push(params[i]);
    }
    ar.push(fn);
    return ar;
  }

  //console.log(params);
  //console.log(args);

  //console.log(Object.keys(args[3])[0])
  //console.log(Object.keys(args[0])[0]);
  function next(i) {
    if (i === args.length) {
      return function (err, data) {
        if (err) {
          console.log(err);
        } else {
          console.log('done');
        }
      };
    }
    return args[i].name = args[i].name.bind(undefined, args[i].params, getFun(next(i+1)));
  }
  
  //next(0);

  var l = args.length-1;
  // this has to come before the loop
  args[l].name = args[l].name.tangle(undefined, pack(args[l].params, final));

  for (var i = l - 1; i >= 0; i--) {
    args[i].name = args[i].name.tangle(undefined, pack(args[i].params, getFun(args[i + 1].name)));
  }

  args[0].name.apply();

  function getFun(fn) {
    return function (err, data) {
      if (err) {
        return final(err);
      } else {
        return fn.apply();
      }
    }
  }

  // added a default error handler here, not sure why
  function errorHandler (err) {
    console.log("error handled here", err);
  }
}

exports.ripple = ripple;
